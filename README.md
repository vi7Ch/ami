# AMi: Application Monitoring i

[![built with Groovy2.8](https://img.shields.io/badge/built%20with-Groovy-red.svg)](http://www.groovy-lang.org)
[![built with Java8](https://img.shields.io/badge/built%20with-Java-green.svg)](https://www.java.com/en/)

> **DISCLAIMER**
    THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


### Version: 0.1 - release 26/12/2017 - Viseth Chaing

Current supported environment: Windows / Linux  
Support LDAP multi domaine, with default to local db user
Support alert via email


> **Deployment** 
- Clone this repo. 
- Unzip and navigate to the content dir. 
- Modify according to your environment the default MMiConfig.yml file. 
Note: a temporary license comes with this repo


### Execution:
The default named 'MMiConfig.yml' file in the current directory is used and a 'logs' directory is created with the logfile 

```bash
java -jar ami-0.1.war

or with java options

java -jar -Xms1G -Xmx1G -XX:+HeapDumpOnOutOfMemoryError -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:gc.log ami-0.1.war
```


To encrypt a password used in the Selenuim scenario copy/paste the output of the below command

```bash
java -jar -DpwdEncrypt=<myNewPwd> exec/AMiCli.jar
``` 

Then use the encrypted string in the MMiDecrypt fonction inside your Selenuim script
```bash
MMiDecrypt('BJ7DZlsbzN57rRA/OOWQYQ==')
``` 


### Note:
You can either create your own Selenium scenarii or use existing scenarii.
Your Selenuim scenarii can be very simple or very complexe.
Whenever a scenario fails, a screenshot at the failed step will be taken and accessible at any given time later.
In order to make sure the site is down, AMi can perform several more checks. The number and interval between retries is customizable.
If after the business service is still down, it sends an alert. An event is logged, the dashboard is updated accordingly and an alert is sent (by emails or Text Message).


**Default admin user**
- local user
user:admin
pwd:password
- LDAP web user
user: euler
pwd:password

Access the user interface to add new user.


**Required INPUT files**
- A CONFIG file named 'MMiConfig.yml' with correct info inside
- the license files: license.out & pubring.gpg


**Logfiles dir**

- General Logfiles: 

```bash
        - LogLevel: INFO, WARN, ERROR, DEBUG
        - LogSize: 20MB
        - LogRotate: 5 files
        - Directory of the logfile: 'logs/' 
        - Name: 'AMi.log'

To turn debugging mode so that debugging lines will appear in the logfile:
java -Dapp.env=DEBUG AMi.jar

```

- Specific scenarii Logfiles:  

```bash
        - LogLevel: INFO, WARN, ERROR, DEBUG
        - LogSize: 20MB
        - LogRotate: 5 files
        - Directory of the logfile: 'data/AMi/<title>/logs/' 
        - Name: 'AMiCli.log'

```


**Screenshot on error**
- the last screenshot on error is accessible via the webUI. look for the camera icon:
other screenshots are saved in the below dir:

```bash
data/AMi/<title>/ScreenShots/
```


